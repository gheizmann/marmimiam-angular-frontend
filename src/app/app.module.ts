import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BarreMenuComponent } from './vue/barre-menu/barre-menu.component';
import { AuthService } from './services/auth.service';

import { HttpClientModule } from '@angular/common/http';
import { AuthGuardService } from './services/auth-guard.service';
import { FourOhFourComponent } from './vue/four-oh-four/four-oh-four.component';
import { RouterModule } from '@angular/router';
import { SigninComponent } from './vue/connexion/signin/signin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignoutComponent } from './vue/connexion/signout/signout.component';
import { RecettesComponent } from './vue/global/recettes/recettes.component';
import { SingleRecetteComponent } from './vue/global/recettes/single-recette/single-recette.component';
import { MesRecettesComponent } from './vue/user/mes-recettes/mes-recettes.component';
import { RecettesService } from './services/recettes.service';
import { SignupComponent } from './vue/connexion/signup/signup.component';
import { InscriptionService } from './services/inscription.service';
import { NouvelleRecetteComponent } from './vue/user/nouvelle-recette/nouvelle-recette.component';
import { NewRecetteService } from './services/new-recette.service';
import { UniteMesureService } from './services/unite-mesure.service';
import { NoterRecetteComponent } from './vue/user/noter-recette/noter-recette.component';
import { GestionUsersComponent } from './vue/admin/gestion-users/gestion-users.component';
import { AuthGuardAdminService } from './services/auth-guard-admin.service';

const appRoutes = [
  { path: 'signin', component: SigninComponent },
  { path: 'signout', component: SignoutComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'mes-recettes', canActivate: [AuthGuardService], component: MesRecettesComponent },
  { path: 'nouvelle-recette', canActivate: [AuthGuardService], component: NouvelleRecetteComponent },
  { path: 'noter-recette/:id', canActivate: [AuthGuardService], component: NoterRecetteComponent },
  { path: 'admin', canActivate: [AuthGuardService, AuthGuardAdminService], component: GestionUsersComponent },
  { path: '', component: RecettesComponent },
  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    BarreMenuComponent,
    FourOhFourComponent,
    SigninComponent,
    SignoutComponent,
    RecettesComponent,
    SingleRecetteComponent,
    MesRecettesComponent,
    SignupComponent,
    NouvelleRecetteComponent,
    NoterRecetteComponent,
    GestionUsersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    AuthGuardService,
    AuthGuardAdminService,
    RecettesService,
    InscriptionService,
    NewRecetteService,
    UniteMesureService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
