/**
 * @class UtilisateurDto
 * 
 * @description Modèle de données DTO pour les requêtes d'authentification
 */
export class UtilisateurDto {
    /**
     * Le login à envoyer pour l'authentification
     */
    public login!: string;
    /**
     * Le mot de passe à envoyer pour l'authentification
     */
    public password!: string;
}
