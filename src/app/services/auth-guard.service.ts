import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { BACKEND_URL, JWT_NAME } from '../config/config';
import { AuthService } from './auth.service';

/**
 * Service permettant le contrôle des routes par rapport à l'authentification
 * en cours
 */
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    var ret: Observable<boolean> =
    this.http.get<boolean>(BACKEND_URL + '/isvalidtoken/' + localStorage.getItem(JWT_NAME));

    ret.toPromise()
    .then(
      (value) => {
        if(value === false) {
          this.authService.seDeconnecter();
        }
      }
    );

    if(!this.authService.estConnecte()) {
      this.router.navigate(['']);
    }

    return ret;
  }
}
