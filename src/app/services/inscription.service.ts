import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { BACKEND_URL } from '../config/config';
import { UtilisateurDto } from './utilisateur-dto';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {
  private reponse!: string;
  reponseSubject = new Subject<string>();

  constructor(
    private http: HttpClient
  ) {}

  emitReponseSubject(): void {
    this.reponseSubject.next(this.reponse);
  }

  inscrire(utilisateurDto: UtilisateurDto): void {
    const header = new HttpHeaders().set('Content-Type', 'application/json');
      
    this.http.put(BACKEND_URL + '/signup',
    utilisateurDto,
    {
      headers: header,
      responseType: 'text'
    }).toPromise()
    .then((value) => {
      this.reponse = value;
      this.emitReponseSubject();
    }).catch((reason) => {
      console.log('ERREUR !!! ' + reason);
    });
  }
}
