export class RecetteDto {
    public id?: number;
    public login?: string;
    public dateMiseEnLigne?: Date;

    public titre!: string;
    public categorie!: string;
    public urlImage?: string;
    public dureePreparation?: number;
    public dureeCuisson?: number;
    public difficulte!: number;
    public prix!: number;
    public texte!: string;
    public listeIngredients: {
        nom: string,
        quantite: number,
        unite: string
    }[] = [];
}
