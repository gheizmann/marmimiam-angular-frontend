import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { BACKEND_URL, JWT_NAME } from '../config/config';
import { Authentification } from './authentification';
import { UtilisateurDto } from './utilisateur-dto';

/**
 * Ce service permet (par souscription au subject) de savoir si l'on est connecté ou
 * pas - par l'intermédiaire du JWT stocké dans le localStorage
 * à l'item JWT_NAME.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  /**
   * La variable stockant l'état de l'authentification
   */
  private authentification: Authentification = new Authentification();
  /**
   * Le subject permettant de connaître l'état de l'authentification
   */
  public authentificationSubject = new Subject<Authentification>();

  /**
   * Constructeur.
   * 
   * Il vérifie si un JWT est stocké dans le localStorage. Si un token valide
   * s'y trouve, authentification.isAuth et authentification.login
   * sont mis à jour à la bonne valeur, sinon on les met respectivement aux valeurs
   * false et ''
   * 
   * @param http Le service permettant de faire des requêtes HTTP
   * @param router Injection du router pour les redirections en cas d'authentification réussie
   */
  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    const token = localStorage.getItem(JWT_NAME);
    if(token !== null) {
      this.http.get<boolean>(BACKEND_URL + '/isvalidtoken/' + token)
      .toPromise()
      .then((value) => {
        if(value === false) {
          this.seDeconnecter();
        }
        else {
          this.authentification.login = JSON.parse(atob(token.split('.')[1])).sub;
          const roles: string = JSON.parse(atob(token.split('.')[1])).roles;
          this.authentification.roles = roles.split(',');
          this.authentification.isAuth = true;
          this.authentification.isInvalide = false;
        }
        this.emitAuthentificationSubject();
      },
      (error) => {
        console.log('JWT invalide' + error);
        this.seDeconnecter();
      });
    }
  }

  /**
   * Permet de mettre à jour la valeur de authentificationSubject
   */
  emitAuthentificationSubject(): void {
    this.authentificationSubject.next(this.authentification);
  }

  /**
   * Méthode appelée lors d'une connexion.
   * 
   * Lorsque la méthode est appelée, appel de la requête '/signin',
   * et mise à jour de JWT_NAME et de authentification si authentification réussie.
   * 
   * @param utilisateurDto Un objet modélisant le JSON à envoyer à la requête de connexion
   * @param redirectTo La redirection à effectuer si la connexion a réussi
   */
  seConnecter(utilisateurDto: UtilisateurDto, redirectTo: string): void {
    const header = new HttpHeaders().set('Content-Type', 'application/json');

    this.http.post(BACKEND_URL + '/signin',
    utilisateurDto,
    {
      headers: header,
      responseType: 'text'
    })
    .toPromise()
    .then((value) => {
      localStorage.setItem(JWT_NAME, value);
      this.authentification.isAuth = true;
      this.authentification.login = utilisateurDto.login;
      this.authentification.isInvalide = false;
      const roles: string = JSON.parse(atob(value.split('.')[1])).roles;
      this.authentification.roles = roles.split(',');
      this.emitAuthentificationSubject();
      this.router.navigate([redirectTo]);
    },
    (error) => {
      console.log('Erreur ! ' + error);
      this.authentification.isInvalide = true;
      this.emitAuthentificationSubject();
    });
  }

  /**
   * Méthode appelée pour se déconnecter du service - efface le JWT et
   * réinitialise authentification
   */
  seDeconnecter(): void {
    localStorage.removeItem(JWT_NAME);
    this.authentification.isAuth = false;
    this.authentification.login = '';
    this.authentification.isInvalide = false;
    this.authentification.roles = [];
    this.emitAuthentificationSubject();
  }

  /**
   * Vérifie si l'utilisateur est connecté
   * 
   * @returns true si l'utilisateur est connecté, false sinon
   */
  estConnecte(): boolean {
    return this.authentification.isAuth;
  }

  estAdmin(): boolean {
    return this.authentification.roles.includes('ROLE_ADMIN');
  }
}
