import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { BACKEND_URL } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class UniteMesureService {
  private unites = new Map<string, string>();
  unitesSubject = new Subject<Map<string, string>>();

  constructor(
    private http: HttpClient
  ) {
    this.http.get(BACKEND_URL + '/recette/liste-unites')
    .toPromise()
    .then(
      (value) => {
        Object.entries(value).forEach(([key,val]) => {
          this.unites.set(key, val);
        });
        this.emitUniteSubject();
      }
    )
  }

  emitUniteSubject(): void {
    this.unitesSubject.next(this.unites);
  }
}
