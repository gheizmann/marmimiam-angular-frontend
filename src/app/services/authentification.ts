/**
 * @class Authentification
 * @description Classe permettant de connaître l'utilisateur connecté, et si un utilisateur
 * est connecté ou non
 */
export class Authentification {
    /**
     * @description Le login du user connecté (=== '' si pas d'user connecté)
     */
    public login: string = '';
    /**
     * @description true si un user est connecté, false sinon
     */
    public isAuth: boolean = false;
    /**
     * @description true si la demande de connexion est invalide, false sinon
     */
    public isInvalide: boolean = false;
    /**
     * @description tableau des rôles de l'user connecté
     */
    public roles: string[] = [];
}
