import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BACKEND_URL, JWT_NAME } from '../config/config';
import { RecetteDto } from './recette-dto';
import { RecettesService } from './recettes.service';

@Injectable({
  providedIn: 'root'
})
export class NewRecetteService {

  constructor(
    private http: HttpClient,
    private recettesService: RecettesService
  ) { }

  ajoutNewRecette(recetteDto: RecetteDto): void {
    const header = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem(JWT_NAME));

    this.http.put(BACKEND_URL + '/user/recette/nouvelle',
    recetteDto,
    {
      headers: header,
      responseType: 'text'
    }).toPromise()
    .then((value) => {
      console.log('Ajout d\'une recette en base de données');
      console.log(value);
    });
  }

  supprimerRecette(id: number, login: string): void {
    const header = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem(JWT_NAME));

    this.http.put(BACKEND_URL + '/user/recette/supprimer/' + id,
    {},
    {
      headers: header,
      responseType: 'text'
    }).toPromise()
    .then((value) => {
      console.log(value);
      this.recettesService.obtenirRecetteUtilisateur(login);
    },
    (error) => {
      console.log('Une erreur est survenue !');
      console.log(error);
    });
  }
}
