import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { RecetteDto } from './recette-dto';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Recette } from '../model/recette';
import { BACKEND_URL, JWT_NAME } from '../config/config';
import { NoteDto } from './note-dto';
import { Notation } from '../model/notation';

@Injectable({
  providedIn: 'root'
})
export class RecettesService {
  private recettes: RecetteDto[] = [];
  recetteSubject = new Subject<RecetteDto[]>();

  constructor(
    private http: HttpClient
  ) { }

  emitRecetteSubject(): void {
    this.recetteSubject.next(this.recettes.slice());
  }

  lireDernieresRecettes(nb: number): void {
    this.http.get<Recette[]>(BACKEND_URL + '/recette/voir/dernieres/' + nb)
    .toPromise()
    .then((value) => {
      this.recettes = [];
      for(let rec of value) {
        var newRecette = new RecetteDto();
        newRecette.id = rec.id;
        newRecette.login = rec.utilisateur.login;
        newRecette.dateMiseEnLigne = rec.dateMiseEnLigne;
        newRecette.titre = rec.titre;
        newRecette.difficulte = rec.difficulte;
        newRecette.prix = rec.prix;
        newRecette.dureePreparation = rec.dureePreparation;
        newRecette.dureeCuisson = rec.dureeCuisson;
        newRecette.urlImage = rec.urlImage;
        newRecette.categorie = rec.categorie.nom;
        newRecette.texte = rec.texte;
        for(let ingr of rec.listeIngredient) {
          newRecette.listeIngredients.push({
            nom: ingr.ingredient.nom,
            quantite: ingr.quantite,
            unite: ingr.unite
          });
        }
        this.recettes.push(newRecette);
      }
      this.emitRecetteSubject();
    });
  }

  obtenirRecetteUtilisateur(login: string): void {
    this.http.get<Recette[]>(BACKEND_URL + '/recette/voir/user/' + login)
    .toPromise()
    .then(
      (value) => {
        this.recettes = [];
        for(let rec of value) {
          var newRecette = new RecetteDto();
          newRecette.id = rec.id;

          newRecette.login = rec.utilisateur.login;
          newRecette.dateMiseEnLigne = rec.dateMiseEnLigne;
          newRecette.titre = rec.titre;
          newRecette.difficulte = rec.difficulte;
          newRecette.prix = rec.prix;
          newRecette.dureePreparation = rec.dureePreparation;
          newRecette.dureeCuisson = rec.dureeCuisson;
          newRecette.urlImage = rec.urlImage;
          newRecette.categorie = rec.categorie.nom;
          newRecette.texte = rec.texte;
          for(let ingr of rec.listeIngredient) {
            newRecette.listeIngredients.push({
              nom: ingr.ingredient.nom,
              quantite: ingr.quantite,
              unite: ingr.unite
            });
          }
          this.recettes.push(newRecette);
        }
        this.emitRecetteSubject();
      }
    );
  }

  voirUneRecette(idRecette: number) {
    return this.http.get<Recette>(BACKEND_URL + '/recette/voir/' + idRecette);
  }

  noterUneRecette(noteDto: NoteDto, id: number) {
    const header = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem(JWT_NAME));

    this.http.put(BACKEND_URL + '/user/recette/noter/' + id,
    noteDto,
    {
      headers: header,
      responseType: 'text'
    })
    .toPromise()
    .then((value) => {
      console.log(value);
    });
  }

  obtenirMoyenneRecette(id: number): Observable<number> {
    return this.http.get<number>(BACKEND_URL + '/recette/moyenne/' + id);
  }

  obtenirNotationsRecette(id: number): Observable<Notation[]> {
    return this.http.get<Notation[]>(BACKEND_URL + '/recette/notation/' + id);
  }

  estDejaNotee(id: number): Observable<boolean> {
    const header = new HttpHeaders().set('Content-Type', 'application/json')
    .set('Authorization', 'Bearer ' + localStorage.getItem(JWT_NAME));

    return this.http.get<boolean>(BACKEND_URL + '/user/recette/estdejanotee/' + id,
    {
      headers: header
    });
  }
}
