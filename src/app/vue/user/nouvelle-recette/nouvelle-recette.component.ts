import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { Authentification } from 'src/app/services/authentification';
import { NewRecetteService } from 'src/app/services/new-recette.service';
import { RecetteDto } from 'src/app/services/recette-dto';
import { UniteMesureService } from 'src/app/services/unite-mesure.service';

@Component({
  selector: 'app-nouvelle-recette',
  templateUrl: './nouvelle-recette.component.html',
  styleUrls: ['./nouvelle-recette.component.scss']
})
export class NouvelleRecetteComponent implements OnInit, OnDestroy {
  formulaire!: FormGroup;
  authentification!: Authentification;
  authSubscription!: Subscription;
  unites!: Map<string, string>;
  unitesSubscription!: Subscription;


  constructor(
    private formBuilder: FormBuilder,
    private newRecetteService: NewRecetteService,
    private authService: AuthService,
    private uniteMesureService: UniteMesureService
  ) { }

  ngOnInit(): void {
    this.authSubscription = this.authService.authentificationSubject.subscribe(
      (value) => {
        this.authentification = value;
      }
    );
    this.authService.emitAuthentificationSubject();

    this.unitesSubscription = this.uniteMesureService.unitesSubject.subscribe(
      (value) => {
        this.unites = value;
      }
    );
    this.uniteMesureService.emitUniteSubject();

    this.initFormulaire();
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
    this.unitesSubscription.unsubscribe();
  }

  private initFormulaire(): void {
    this.formulaire = this.formBuilder.group(
      {
        titre: ['', Validators.required],
        categorie: ['', Validators.required],
        urlImage: '',
        dureePreparation: 0,
        dureeCuisson: 0,
        difficulte: 1,
        prix: 1,
        texte: ['', Validators.required],
        ingredients: this.formBuilder.array([])
      }
    );
  }

  getIngredients(): FormArray {
    return this.formulaire.get('ingredients') as FormArray;
  }

  onAddIngredient() {
    const newIngredientControl = this.formBuilder.group({
      nom: ['', Validators.required],
      quantite: [0, Validators.required],
      unite: ['', Validators.required]
    });
    this.getIngredients().push(newIngredientControl);
  }

  onEnvoyerRecette(): void {
    const formValue = this.formulaire.value;
    const newRecette = new RecetteDto();
    newRecette.titre = formValue['titre'];
    newRecette.categorie = formValue['categorie'];
    newRecette.dureePreparation = formValue['dureePreparation'];
    newRecette.dureeCuisson = formValue['dureeCuisson'];
    newRecette.difficulte = formValue['difficulte'];
    newRecette.prix = formValue['prix'];
    newRecette.texte = formValue['texte'];
    newRecette.listeIngredients = formValue['ingredients'];
    newRecette.urlImage = '';

    console.log(newRecette);
    this.newRecetteService.ajoutNewRecette(newRecette);
  }

  onSupprimerIngredient(i: number): void {
    this.getIngredients().removeAt(i);
  }
}
