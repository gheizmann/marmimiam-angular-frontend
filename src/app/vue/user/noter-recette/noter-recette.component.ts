import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Recette } from 'src/app/model/recette';
import { NoteDto } from 'src/app/services/note-dto';
import { RecettesService } from 'src/app/services/recettes.service';

@Component({
  selector: 'app-noter-recette',
  templateUrl: './noter-recette.component.html',
  styleUrls: ['./noter-recette.component.scss']
})
export class NoterRecetteComponent implements OnInit {
  idRecette!: number;
  recette!: Recette;
  noteFormulaire!: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private recettesService: RecettesService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.idRecette = this.route.snapshot.params['id'];
    this.recettesService.voirUneRecette(this.idRecette)
    .toPromise()
    .then((value) => {
      this.recette = value;
    });
  }

  initForm(): void {
    this.noteFormulaire = this.formBuilder.group(
      {
        note: [1, Validators.required],
        commentaire: ''
      }
    );
  }

  onSubmitForm(): void {
    const formValue = this.noteFormulaire.value;
    var newNote = new NoteDto();
    newNote.note = formValue['note'];
    newNote.commentaire = formValue['commentaire'];

    console.log('Nouvelle note : ' + newNote.note);
    console.log('Nouveau commentaire : ' + newNote.commentaire);

    this.recettesService.noterUneRecette(newNote, this.idRecette);
  }

}
