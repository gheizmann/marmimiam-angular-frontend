import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { Authentification } from 'src/app/services/authentification';
import { NewRecetteService } from 'src/app/services/new-recette.service';
import { RecetteDto } from 'src/app/services/recette-dto';
import { RecettesService } from 'src/app/services/recettes.service';

@Component({
  selector: 'app-mes-recettes',
  templateUrl: './mes-recettes.component.html',
  styleUrls: ['./mes-recettes.component.scss']
})
export class MesRecettesComponent implements OnInit, OnDestroy {
  recettes!: RecetteDto[];
  recetteSubscription!: Subscription;

  authentification!: Authentification;
  authSubscription!: Subscription;

  constructor(
    private recettesService: RecettesService,
    private newRecetteService: NewRecetteService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authSubscription = this.authService.authentificationSubject.subscribe(
      (value) => {
        this.authentification = value;
      }
    );
    this.authService.emitAuthentificationSubject();
    this.recetteSubscription = this.recettesService.recetteSubject.subscribe(
      (value) => {
        this.recettes = value;
      }
    );
    this.recettesService.emitRecetteSubject();
    if(this.authentification.isAuth === true) {
      this.recettesService.obtenirRecetteUtilisateur(this.authentification.login);
    }
  }

  ngOnDestroy(): void {
    this.recetteSubscription.unsubscribe();
    this.authSubscription.unsubscribe();
  }

  onSupprimerRecette(id: number | undefined): void {
    if(id === undefined) {
      console.log('Id de recette non défini');
    }
    else {
      if(confirm("Voulez-vous vraiment supprimer la recette ?")) {
        this.newRecetteService.supprimerRecette(id, this.authentification.login);
      }
    }
  }

}
