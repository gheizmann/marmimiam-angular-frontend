import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Notation } from 'src/app/model/notation';
import { AuthService } from 'src/app/services/auth.service';
import { RecetteDto } from 'src/app/services/recette-dto';
import { RecettesService } from 'src/app/services/recettes.service';

@Component({
  selector: 'app-single-recette',
  templateUrl: './single-recette.component.html',
  styleUrls: ['./single-recette.component.scss']
})
export class SingleRecetteComponent implements OnInit {
  @Input() recette!: RecetteDto;
  moyenne!: Observable<number>;
  notations!: Notation[];
  estNotee!: boolean;

  estNotable: boolean = false;

  constructor(
    private authService: AuthService,
    private recettesService: RecettesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if(this.authService.estConnecte()) {
      this.estNotable = true;
    }
    if(this.recette.id !== undefined) {
      this.moyenne = this.recettesService.obtenirMoyenneRecette(this.recette.id);
      this.recettesService.obtenirNotationsRecette(this.recette.id)
      .toPromise()
      .then((value) => {
        this.notations = value;
      });
      this.recettesService.estDejaNotee(this.recette.id)
      .toPromise()
      .then((value) => {
        this.estNotee = value;
      });
    }
  }

  onNoterRecette(): void {
    this.router.navigate(['noter-recette', this.recette.id]);
  }

}
