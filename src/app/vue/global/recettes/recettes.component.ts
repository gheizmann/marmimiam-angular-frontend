import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { RecetteDto } from 'src/app/services/recette-dto';
import { RecettesService } from 'src/app/services/recettes.service';

@Component({
  selector: 'app-recettes',
  templateUrl: './recettes.component.html',
  styleUrls: ['./recettes.component.scss']
})
export class RecettesComponent implements OnInit, OnDestroy {
  recettes: RecetteDto[] = [];
  recetteSubscription!: Subscription;

  constructor(
    private recettesService: RecettesService
  ) { }

  ngOnInit(): void {
    this.recetteSubscription = this.recettesService.recetteSubject.subscribe(
      (value) => {
        this.recettes = value;
      }
    );
    this.recettesService.emitRecetteSubject();
    this.onVoirDernieresRecettes();
  }

  ngOnDestroy(): void {
    this.recetteSubscription.unsubscribe();
  }

  onVoirDernieresRecettes(): void {
    this.recettesService.lireDernieresRecettes(3);
  }

}
