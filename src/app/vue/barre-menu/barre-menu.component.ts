import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { Authentification } from 'src/app/services/authentification';

/**
 * @class BarreMenuComponent
 * 
 * Ce component gère la barre de menu située dans le header de la SPA.
 */
@Component({
  selector: 'app-barre-menu',
  templateUrl: './barre-menu.component.html',
  styleUrls: ['./barre-menu.component.scss']
})
export class BarreMenuComponent implements OnInit, OnDestroy {
  authentification!: Authentification;
  authSubscription!: Subscription;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authSubscription = this.authService.authentificationSubject.subscribe(
      (value) => {
        this.authentification = value;
      }
    );
    this.authService.emitAuthentificationSubject();
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

}
