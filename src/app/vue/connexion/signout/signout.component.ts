import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { Authentification } from 'src/app/services/authentification';

@Component({
  selector: 'app-signout',
  templateUrl: './signout.component.html',
  styleUrls: ['./signout.component.scss']
})
export class SignoutComponent implements OnInit, OnDestroy {
  authentification!: Authentification;
  authSubscription!: Subscription;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.authSubscription = this.authService.authentificationSubject.subscribe(
      (value) => {
        this.authentification = value;
      }
    );
    this.authService.emitAuthentificationSubject();
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

  onSeDeconnecter(): void {
    this.authService.seDeconnecter();
    this.router.navigate(['']);
  }
}
