import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { Authentification } from 'src/app/services/authentification';
import { UtilisateurDto } from 'src/app/services/utilisateur-dto';

/**
 * Ce component permet l'authentification au site (backend)
 */
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit, OnDestroy {
  formulaire!: FormGroup;
  authentification!: Authentification;
  authSubscription!: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.formulaire = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.authSubscription = this.authService.authentificationSubject.subscribe(
      (value) => {
        this.authentification = value;
      }
    );
    this.authService.emitAuthentificationSubject();
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

  onSeConnecter(): void {
    const val = this.formulaire.value;
    var utilisateurDto = new UtilisateurDto();
    utilisateurDto.login = val.login;
    utilisateurDto.password = val.password;
    this.authService.seConnecter(utilisateurDto, '');
  }
}
