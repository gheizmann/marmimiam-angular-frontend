import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { InscriptionService } from 'src/app/services/inscription.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  reponse!: string;
  reponseSubscription!: Subscription;
  formulaire!: FormGroup;

  constructor(
    private inscriptionService: InscriptionService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.reponseSubscription = this.inscriptionService.reponseSubject.subscribe(
      (value) => {
        this.reponse = value;
      }
    );
    this.inscriptionService.emitReponseSubject();
    this.creerFormulaire();
  }

  ngOnDestroy(): void {
    this.reponseSubscription.unsubscribe();
  }

  private creerFormulaire(): void {
    this.formulaire = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onCreerCompte(): void {
    const val = this.formulaire.value;
    this.inscriptionService.inscrire(val);
  }

}
