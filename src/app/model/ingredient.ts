export class Ingredient {
    public recetteId!: number;
    public ingredientId!: number;
    public ingredient!: {
        id: number,
        nom: string
    };
    quantite!: number;
    unite!: string;
}
