import { Recette } from './recette';

export class Notation {
    public recetteId!: number;
    public utilisateurId!: number;
    public recette!: Recette;
    public utilisateur!: {
        login: string
    };
    public note!: number;
    public commentaire?: string;
    public dateNotation!: Date;
}
