import { Ingredient } from './ingredient';

export class Recette {
    public id!: number;
    public titre!: string;
    public dateMiseEnLigne!: Date;
    public categorie!: {
        id: number,
        nom: string
    };
    public urlImage?: string;
    public dureePreparation?: number;
    public dureeCuisson?: number;
    public difficulte!: number;
    public prix!: number;
    public texte!: string;
    public utilisateur!: {
        login: string
    };
    public listeIngredient!: Ingredient[];
}
